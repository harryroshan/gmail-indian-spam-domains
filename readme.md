### Introduction



Anytime one does one of teh following in India, some mass-spam companies scrape email addresses from publiuc records & bombard one with literally 10+ emails a day.

* Register a domain name
* Register a company
* Unsubscribe from [Connect Broadband Punjab](http://www.connectzone.in/)
* Give email address in Shopping Malls surveys

This is a list of those domains, to be used in Gmail filter, to auto delete those emails & push to Recycle Bin.

### How Spam Email Works

All most all of these spam emails have same layout; horizontally centered, an image or two, & then an Unsubscribe link at bottom.

All links, Images point to a specific hashed URL, identifying YOUR email. Anytime any image is loaded, or link is clicked, They know & register it & mark one's email as Hot/Live. yes, even if you clicked unsubscribe, you will NOT be unsubscribed. Every loaded image or clicked link will result in 10x more spam than earlier.

Sample Hashed URL_("X" added by me)_:  
`http://ltg.crm.utrans01.com/ltrack?g=1&id=KUUXXXXXXXXXXXXXXXXXVFNcUxX=V1XXXXXXXXXXXXXQC1dWA0t3XXXXXXXXXXte&client=XXXX7`

### Sample Subject Lines

* Facing a Financial crunch? Get instant approved funds @ 10.75%
* Rolex Daytona Full GoldWatch @ 79%Off COD Available
* Choose the best credit card that suits you best.
* Get Rs 4 lakhs Health cover starting @ Rs 12/day*
* Are You a Blockchain Enthusiasts? Get Certified Today

### Sample Email Screenshots

_TODO_

### What to do?

The email address cannot be removed from these lists; as multiple copies of these lists get sold by multiple criteria & LIVE/HOT emails fetch more money.

Few steps to be taken as precaution: 
* Registering a new domain or a new company? Your details will be in the public record. Try to use a privacy service. If Privacy Service not allowed (like on .in domains), try to use a dedicated email address.
* Try to use a burner/trash/temporary email address for promotions etc.

### How to use this list of domains?

Gmail has a filter which says `if email received from THIS domain, never mark it as important, delete it.` & Filter also takes OR keyword i.e. from: (`spam.com OR fish.can`) will match emails from EITHER one of these

#### Why delete & why not report spam?  
Because any unread email in Spam folder will cause a counter to shown in folder list i.e. `Spam(45)`, whereas any unread/read emails in Bin does not show the counter.

#### Contents
This repo contains two files:

**spam-domains-list.txt**: List of all domains in Human Readable Format  
**spam-domains-list.dist.txt** List of all domains, ready to be copy pasted in Gmail Filter Field

> 101coupon.in OR 24minds.in OR 32.mailer.ideascost.com OR airpostmail.in OR backendmails.com OR bankmarket.in OR buzz-india.com OR carnivalcinemas.com OR check2inbox.com OR cloudbazaar.org OR cm.redirectify.in OR crop.infosysworld.com OR deals-on-mail.com OR displaymailbox.com OR emaily.in OR findstay.in OR freedealcode.in OR freekacharge.com OR hotoffers.co.in OR lucifro.com OR m2i.in OR mail7.dealsmall.in OR mailerassist.com OR mailmx.mazewebpromotes.net OR mailonspot.com OR mailscart.com OR mc.shiningindiaa.com OR mojodesks.in OR mweb.co.in OR nrimb.com OR offer4uhub.com OR offeronmail.com OR robomart.com OR smtpmailbox.com OR sp.endmile.co.in OR walmart.com OR zenxmail.com


#### Instructions (Step By Step)

1. Go to Gmail Settings & then "Filters & Blocked Addresses"
1. Click "Create a New Filter" at the bottom of the page.
1. Copy the whole prepared String from spam-domains-list.dist.txt & paste in "From" field.
1. Click Continue
1. Tick the boxes "Delete It" & "Never mark it as important"
1. Click "Create Filter" or "Update Filter"

### TODO

[ ] Add sample email screenshots.  
[ ] Add my own stats of spam emails received.  
[ ] Convert the human readable list to tabular format .md  
[x] Move the dist.txt to a box here in readme itself  2018-03-03  
[ ] Add Whois details of these spam domains  
[ ] Start using Issues for TODO


#### Contribution

All PRs, Issues are welcome.